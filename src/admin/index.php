<div id='um-admin-page'>

  <div class='header'>
    User Monit
  </div>

  <div class='desc'>
    Below you will find a list of all recorded successful user login/logout attempts along with their related event information.
  </div>


  <table id='um-main-table'>
    <tr>
      <th>User</th>
      <th>Action</th>
      <th>IP Address</th>
      <th>User Agent String</th>
      <th>Date</th>
    </tr>

    <?php

    $row_count = 0;

    foreach( $logs as $log )
    {
      $date = gmdate('M j Y g:ia',strtotime($log->created));
      $action = $actions[ $log->action_id ]; 
      $row_class = ( $row_count++ %2 == 0 ? 'even' : 'odd');
      $edit_href = get_edit_user_link( $log->user_id );

      echo "
        <tr class='{$row_class}'>
          <td class='username'><a href='{$edit_href}'>{$log->user_login}</a></td>
          <td class='action'>{$action}</td>
          <td class='ip_addr'>{$log->ip_addr}</td>
          <td class='ua_string'>{$log->browser}</td>
          <td class='date'>{$date}</td>
        </tr>
      ";
    }

    ?>
  </table>

</div>