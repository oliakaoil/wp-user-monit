<?php

// https://developer.wordpress.org/reference/functions/register_uninstall_hook/
if(!defined('WP_UNINSTALL_PLUGIN') || !WP_UNINSTALL_PLUGIN)
  exit;

define('PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

global $wpdb;
require_once( PLUGIN_PATH 'class.usermonit.php' );

$umInst = new Usermonit( $wpdb );

$umInst->uninstall();