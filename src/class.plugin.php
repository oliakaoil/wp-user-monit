<?php

class Plugin {

  protected $table_prefix;
  protected $wpdb;

  public function __construct( $wpdb ) {

    $this->wpdb =& $wpdb;
  }

  public function activate(){

    $sql = $this->getSql();   

    foreach( $sql['activate'] as $activate_sql ) {
      $this->wpdb->query( $activate_sql ); 
    }

  }

  public function deactivate(){
    $sql = $this->getSql();

    foreach( $sql['deactivate'] as $uninstall_sql ){
      $this->wpdb->query( $uninstall_sql );
    }    
  }

  public function uninstall(){

    $sql = $this->getSql();

    foreach( $sql['uninstall'] as $uninstall_sql ){
      $this->wpdb->query( $uninstall_sql );
    }
  }

  protected function getSql(){

    static $sql;

    if(!$sql) {
      $table_prefix = $this->table_prefix;
      $charset_collate = $this->wpdb->get_charset_collate();
      $sql = include( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'sql.php' );
    }

    return $sql;
  }
}