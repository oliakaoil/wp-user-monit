<?php

require_once( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'class.plugin.php' );

class Usermonit extends Plugin {

  const ACTION_LOGIN = 1;
  const ACTION_LOGOUT = 2;
  const ACTION_PWRESET = 3;

  public function __construct( $wpdb ) {

    parent::__construct( $wpdb );

    $this->table_prefix = $this->wpdb->prefix . 'usermonit_';
  }

  public function getLog( $limit = 50 , $offset = 0 ){

    $table_name = $this->table_prefix . "log";
    $limit = (int) $limit;
    $offset = (int) $offset;

    $sql = "
      SELECT log.*,user.user_login 
      FROM {$table_name} AS log 
      LEFT JOIN {$this->wpdb->users} AS user ON user.id = log.user_id
      ORDER BY log.created DESC
      LIMIT {$offset},{$limit}
    ";

    return $this->wpdb->get_results( $sql );
  }

  public function getActions(){
    return array( 
      self::ACTION_LOGIN => 'login', 
      self::ACTION_LOGOUT => 'logout', 
      self::ACTION_PWRESET => 'password reset'
      );
  }

  public function logAction( $action_id , $user_id ) {

    if(empty($action_id) || empty($user_id))
      return 1;

    $actions = array_keys( $this->getActions() );

    if(!in_array( $action_id , $actions))
      return 2;

    $data = array(
      'user_id' => (int) $user_id,
      'action_id' => (int) $action_id,
      'ip_addr' => (string) $this->getIpAddr(),
      'browser' => $this->getBrowserId(),
      'created' => gmdate('Y-m-d H:i:s')
      );

    $table_name = $this->table_prefix . "log";

    $this->wpdb->insert( $table_name , $data );

    return 0;
  }

  protected function getBrowserId() {
    if(empty($_SERVER['HTTP_USER_AGENT']))
      return;

    return $_SERVER['HTTP_USER_AGENT'];
  }

  // http://stackoverflow.com/questions/6717926/function-to-get-user-ip-address
  protected function getIpAddr() {
    static $addr;

    if( $addr )
      return $addr;

    $server_keys = array(
      'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 
      'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR'
      );

    foreach( $server_keys as $key) {

      if (array_key_exists($key, $_SERVER) === true) {

        foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip) {

          if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
            $addr = $ip;
            return $ip;
          }
        }
      }
    }
  }
}