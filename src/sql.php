<?php

$sql = array(
  'activate' => array() , 
  'deactivate' => array(), 
  'uninstall' => array()
  );

$sql['activate'][] = "
  CREATE TABLE IF NOT EXISTS {$table_prefix}log (
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    user_id INT UNSIGNED NOT NULL,
    action_id INT UNSIGNED NOT NULL,
    ip_addr VARCHAR(15) NOT NULL,
    browser VARCHAR(100) NOT NULL,
    created DATETIME NOT NULL,
    KEY `user_id_action_id` (`user_id`,`action_id`)
  ) {$charset_collate};
  ";

$sql['uninstall'][] = "DROP TABLE IF EXISTS {$table_prefix}log";

return $sql;
