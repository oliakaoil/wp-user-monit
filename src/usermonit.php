<?php

/*
 * Plugin Name: User Monit
 * Plugin URI: https://bitbucket.org/oliakaoil/wp-user-monit
 * Description: Tracks user login/logout activity. Attaches IP address, user agent and datetime, and display list view in the admin screen.
 * Version: 0.1
 * Author: Oliver Gould
 * Author URI: https://bitbucket.org/oliakaoil
 * License: GPLv2 or later
 * Text Domain: User-monit
 */

if ( !function_exists( 'add_action' ) )
  exit;

define('UM_PLUGIN_NAME','usermonit');
define('UM_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

global $wpdb;
require_once( UM_PLUGIN_PATH . 'class.usermonit.php' );
$umInst = new Usermonit( $wpdb );


/*
 * Plugin management hooks
 */

// https://developer.wordpress.org/reference/functions/register_activation_hook
register_activation_hook( __FILE__ , array( $umInst , 'activate' ) );


// https://developer.wordpress.org/reference/functions/register_deactivation_hook
register_deactivation_hook( __FILE__ , array( $umInst , 'deactivate' ) );


add_action( 'admin_menu', function() use( $umInst ) {
  add_users_page('User Monit options', 'User Monit', 'manage_options', 'user-monit', function() use( $umInst ) {

    if ( !current_user_can( 'manage_options' ) )
      wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    
    $actions = $umInst->getActions();
    $logs = $umInst->getLog();

    wp_enqueue_style('user-monit-style', plugins_url( '/admin/style.css', __FILE__ ), array(),'all');

    require_once( UM_PLUGIN_PATH . 'admin/index.php');

  });
});



/*
 * WP activity tracking hooks
 */

add_action('wp_login', function( $username , $wp_user ) use( $umInst ) {

  $user_id = ( $wp_user->id ? $wp_user->id : $wp_user->ID );
  $umInst->logAction( Usermonit::ACTION_LOGIN , $user_id );

} , 10 , 2);

add_action('wp_logout', function() use( $umInst ) {

  $wp_user = wp_get_current_user();
  $user_id = ( $wp_user->id ? $wp_user->id : $wp_user->ID );

  $umInst->logAction( Usermonit::ACTION_LOGOUT  , $user_id );
});

add_action('retrieve_password', function() use( $umInst ) {

  $wp_user = wp_get_current_user();
  $user_id = ( $wp_user->id ? $wp_user->id : $wp_user->ID );

  $umInst->logAction( Usermonit::ACTION_PWRESET , $user_id );
});


