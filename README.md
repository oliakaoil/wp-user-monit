
## Overview ##

The User Monit Wordpress plugin keeps track of successful user logins and logouts to the Wordpress admin. Each action is recorded along with the IP address, date and time and user agent string in an effort to more closely monitor user activity.

## Installation ##

This plugin was built to work with Wordpress version 4.7.2, PHP 5.6.29 and Apache. It has not been tested with any other versions. The plugin resides in the src folder and can be installed using the expected method, which is to copy the plugin into the Wordpress plugin directory and name it accordingly:

    # cp src /path/to/wordress/wp-content/plugins/user-monit

The plugin can then be activated/deactivated etc. as expected through the Wordpress plugin admin. After being activated, there will be a submenu named 'User Monit' in the 'Users' admin menu. For ease of use during local development it is strongly advised that you link the src folder rather than copying it directly:

    # cd /path/to/wordpress/wp-content/plugins
    # ln -s /path/to/src ./user-monit

## Further work ##
 
 This is a simple example of a Wordpress plugin and many additional features have been left out:
  * There is no significant error or exception handling
  * Features like pagination and filtering are missing from the admin page
  * There is no limit on the number or age of recorded activities or any sort of automated cleanup or maintenance
  * There is no aggregate or single-user view